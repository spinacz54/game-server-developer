package com.wojciech.kuchno.boxgame;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GameTest {
    @Test
    public void gameConstructor(){

        Game g = new Game();

        assertEquals(g.getBoxes().size(), 12);
        assertEquals(g.getReward(), 0);
        assertEquals(g.getSecondReward(), 0);
        assertFalse(g.isExtraLife());

    }

    @Test
    public void firstReward(){

        Game g = new Game();
        g.play();

        assertTrue(g.getReward() >= 0);
        assertTrue(g.getReward() <= 165);

        assertTrue(g.getReward() >= 0);
        assertTrue(g.getReward() <= 165);

    }

    @Test
    public void secondReward(){

        Game g = new Game();
        g.play();

        assertTrue(g.getSecondReward() >= 0);
        assertTrue(g.getSecondReward() <= 165);

    }

    @Test
    public void endedGameHasNoExtraLife(){

        Game g = new Game();
        g.play();

        assertFalse(g.isExtraLife());

    }
}
