package com.wojciech.kuchno.boxgame;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CalculationsTest {

    @Test
    public void factorialTest(){

        assertEquals(Calculations.factorial(3), 6);
        assertEquals(Calculations.factorial(4), 24);
        assertEquals(Calculations.factorial(5), 120);

    }

    @Test
    public void newtonTest(){

        assertEquals(Calculations.newton(5,3),10, 0.001);
        assertEquals(Calculations.newton(8,3),56, 0.001);
        assertEquals(Calculations.newton(10,4), 210, 0.001);

    }
}
