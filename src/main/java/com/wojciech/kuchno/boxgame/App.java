package com.wojciech.kuchno.boxgame;

public class App 
{
    public static void main( String[] args )
    {

        System.out.println("Analitical expected value of reward: " + analiticalValueOfExpectedReward());
        System.out.println("Simulation of expected value of reward: " + simulation(10000000));

    }

    /*   just for easier reading   */

    private static double n(int a, int b){
        return Calculations.newton(a,b);
    }

    private static double f(int a){
        return Calculations.factorial(a);
    }

    private static double analiticalValueOfExpectedReward(){

        double v = 165.0/8; // average value of winning cards

        double sum = 0;

        /*   n() stands for Binomial coefficient (Newton's symbol), f() stands for factorial   */

        for(int i = 1; i <= 8; i++){

            sum  += (i * v) * ((n(8,i) * f(i) * 3 * f(12 - (i +1)) +
                    n(8,i) * 3 * n(i + 2, 2) * f(i) * 2 * f(12 - (i + 3))));

        }

        sum /= f(12);
        sum += 0.25*(sum + 0.33 * 5 + 0.33 * 10 + 0.33 * 20);           // case of second chance
        sum += 0.25*5 + 0.25 * 10 + 0.25 * 20;                          // other cases

        return sum;
    }

    private static double simulation(int n){

        double sum = 0;

        for(int i = 0; i < n; i++){
            sum += (new Game()).play();
        }

        return sum / n;
    }
}
