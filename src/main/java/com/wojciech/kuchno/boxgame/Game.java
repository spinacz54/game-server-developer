package com.wojciech.kuchno.boxgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    private List<Box> boxes;
    private boolean extraLife;
    private int reward;
    private int secondReward;

    public Game() {

        List<String> cards = prepareCardsDeck();
        extraLife = false;
        reward = 0;
        secondReward = 0;

        Random gen = new Random();
        boxes = new ArrayList();

        int count = 12;

        for(int i = 12; i > 0; i--){

            int index = gen.nextInt(i);
            boxes.add(new Box(cards.get(index)));
            cards.remove(index);
        }
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public boolean isExtraLife() {
        return extraLife;
    }

    public int getReward() {
        return reward;
    }

    public int getSecondReward() {
        return secondReward;
    }

    public int play(){
        return round(true);
    }

    private int round(boolean secondChanceAllowed){

        if(!secondChanceAllowed){

            List<String> cards = prepareCardsDeck();

            boxes = new ArrayList();

            Random gen = new Random();
            int count = 12;

            for(int i = 0; i < 12; i++){

                int index = gen.nextInt(count-- );
                boxes.add(new Box(cards.get(index)));
                cards.remove(index);

            }
        }

        for(int i = 0; i < 12; i++){

            if(boxes.get(i).getContent() == "Game over"){
                if(!extraLife){
                    break;
                }else{
                    extraLife = false;
                }
            }else if(boxes.get(i).getContent() == "Extra life"){
                extraLife = true;
            }else{
                if (secondChanceAllowed) {
                    reward += Integer.parseInt(boxes.get(i).getContent());
                }else{
                    secondReward += Integer.parseInt(boxes.get(i).getContent());
                }
            }
        }

        if (secondChanceAllowed) {
            return reward + getAdditionalReward(secondChanceAllowed);
        }else{
            return secondReward + getAdditionalReward(secondChanceAllowed);
        }
    }

    private int getAdditionalReward(boolean secondChanceAllowed){
        Random gen = new Random();

        int res = 0;

        if (secondChanceAllowed) {
            switch(gen.nextInt(4)){

                case 0:
                    res = 5;
                    break;
                case 1:
                    res = 10;
                    break;
                case 2:
                    res = 20;
                    break;
                case 3:
                    if (secondChanceAllowed) {
                        res = round(false);
                    }
                    break;
            }
        }else{
            switch(gen.nextInt(3)){

                case 0:
                    res = 5;
                    break;
                case 1:
                    res = 10;
                    break;
                case 2:
                    res = 20;
                    break;
            }
        }
        return res;
    }

    private static List<String> prepareCardsDeck(){
        List<String> cards = new ArrayList();

        cards.add("100");
        cards.add("20");
        cards.add("20");
        cards.add("5");
        cards.add("5");
        cards.add("5");
        cards.add("5");
        cards.add("5");
        cards.add("Extra life");
        cards.add("Game over");
        cards.add("Game over");
        cards.add("Game over");

        return cards;
    }
}
