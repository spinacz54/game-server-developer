package com.wojciech.kuchno.boxgame;

public class Calculations {

    public static int factorial(int x){

        int tmp = 1;

        for(int i = x; i > 0; i--){
            tmp *= i;
        }

        return tmp;
    }

    public static double newton(int n, int k){

        return factorial(n)/(factorial(k)*factorial(n -k));
    }
}
