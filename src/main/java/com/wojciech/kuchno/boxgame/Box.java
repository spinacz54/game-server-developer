package com.wojciech.kuchno.boxgame;

public class Box {

    private String content;

    public Box(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
